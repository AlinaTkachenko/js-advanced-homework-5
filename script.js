const wrapper = document.querySelector('.wrapper');

class Cards {
  constructor(title, text, name, email, postId) {
    this.title = title;
    this.text = text;
    this.name = name;
    this.email = email;
    this.postId = postId;
  }
}

function getUsers() {
  return fetch("https://ajax.test-danit.com/api/json/users")
    .then(response => response.json());
}

function getPosts() {
  return fetch("https://ajax.test-danit.com/api/json/posts")
    .then(response => response.json());
}

function createObjCards(users, posts) {
  console.log(users, posts)
  posts.forEach(post => {
    let name;
    let email;
    users.forEach(user => {
      if (user.id === post.userId) {
        name = user.name;
        email = user.email;
      }
    });

    let card = new Cards(`${post.title}`, `${post.body}`, name, email, `${post.id}`);
    displayCard(card);

  })
}

function displayCard(card) {
  const wrapperForPost = document.createElement('div');
  wrapperForPost.setAttribute('data-id',card.postId);
  wrapperForPost.classList.add('wrapper-card');
  wrapperForPost.innerHTML = `
  <h2>${card.name}</h2>
  <p>${card.email}</p>
  <h3>${card.title}</h3>
  <p>${card.text}</p>`;
  wrapper.append(wrapperForPost);
  const btn = document.createElement('a');
  btn.innerText = 'Delete';
  btn.classList.add('btn');
  btn.setAttribute('id',card.postId);
  btn.addEventListener('click', function (event) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${event.target.id}`, {
      method: 'DELETE',
    })
    .then(response => {
      if(response.status === 200){
        document.querySelector(`div[data-id="${event.target.id}"]`).remove();
      }
    })
  })
  wrapperForPost.append(btn);
};

Promise.all([getUsers(), getPosts()]).then(([users, posts]) => {
  createObjCards(users, posts);
});